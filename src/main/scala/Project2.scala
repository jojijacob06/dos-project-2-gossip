import akka.actor._

import scala.collection.mutable.Queue
import scala.collection.mutable.ListBuffer
import scala.util.Random

sealed trait ProjectTwoMessages
case class BossInit(numNode: Int, top: String, alg: String) extends ProjectTwoMessages
case class MakeAverageJoes(count: Int, topology: String) extends ProjectTwoMessages
case object DoneMakingJoes extends ProjectTwoMessages
case object GossipTermination extends ProjectTwoMessages
case object NeighborListEmpty extends ProjectTwoMessages
case object TickDone extends ProjectTwoMessages
case class StatsResponse(statsTo: List[(Long, String)], statsFrom: List[(Long, String)]) extends ProjectTwoMessages

case class JoeInit(neighborRefs: List[ActorRef], thisJoeId: Int, w: Int) extends ProjectTwoMessages
case object Rumor extends ProjectTwoMessages
case object Tick extends ProjectTwoMessages
case object RemoveFromNeighborList extends ProjectTwoMessages
case object StatsRequest extends ProjectTwoMessages
case class PushSum(s: Float, w: Float) extends ProjectTwoMessages

class Boss extends Actor with ActorLogging {
  var numberOfNodes: Int = _
  var topology: String = _
  var algorithm: String = _
  val averageJoeRefs: ListBuffer[ActorRef] = new ListBuffer[ActorRef]()
  var gossipStartTime: Long = _
  var joesWhereGossipTerminated: ListBuffer[String] = new ListBuffer[String]()
  var joesWhereNeighborListIsEmpty: ListBuffer[String] = new ListBuffer[String]()
  var joesWhereTickDone: Int = 0
  var tickCount: Long = 0
  var statsReceived: Int = 0

  def JoeIdToRef(id: Int): ActorRef = averageJoeRefs(id)
  def JoeRefToId(joe: ActorRef): Int = averageJoeRefs.indexOf(joe)

  def receive = {
    case BossInit(numNodes, top, alg) =>
      numberOfNodes = numNodes
      topology = top
      algorithm = alg
      self ! MakeAverageJoes(numberOfNodes, topology)

    case MakeAverageJoes(count, topology) =>
      topology match {
        case "full" =>
          for (i <- 0 until count) {
            averageJoeRefs += context.system.actorOf(Props(new AverageJoe), s"Joe$i")
          }

          for (i <- 0 until count) {
            val neighbors: ListBuffer[Int] = collection.mutable.ListBuffer.range(0, count)
            neighbors.remove(neighbors.indexOf(i))
            averageJoeRefs(i) ! JoeInit(neighbors.map(JoeIdToRef).toList, i+1, 1)
          }

          self ! DoneMakingJoes

        case "line" =>
          for (i <- 0 until count) {
            averageJoeRefs += context.system.actorOf(Props(new AverageJoe), s"Joe$i")
          }

          for (i <- 0 until count) {
            val neighbors: ListBuffer[Int] = new ListBuffer[Int]()
            if (i == 0) {
              neighbors += 1
            }
            else if (i == count-1) {
              neighbors += count - 2
            }
            else {
              neighbors += i - 1
              neighbors += i + 1
            }
            averageJoeRefs(i) ! JoeInit(neighbors.map(JoeIdToRef).toList, i+1, 1)
          }

          self ! DoneMakingJoes

        case "3d" | "imp3d" =>
          val numberOfNodesPerSide: Int = (scala.math.cbrt(count.toDouble)+0.5).toInt
          numberOfNodes = numberOfNodesPerSide * numberOfNodesPerSide * numberOfNodesPerSide

          for (i <- 0 until numberOfNodes) {
            averageJoeRefs += context.system.actorOf(Props(new AverageJoe), s"Joe$i")
          }

          val validIndices: Range = 0 until numberOfNodesPerSide
          val iFactor: Int = numberOfNodesPerSide
          val jFactor: Int = 1
          val kFactor: Int = numberOfNodesPerSide*numberOfNodesPerSide



          for (k <- 0 until numberOfNodesPerSide) {
            for (i <- 0 until numberOfNodesPerSide) {
              for (j <- 0 until numberOfNodesPerSide) {
                val neighbors: ListBuffer[Int] = new ListBuffer[Int]()
                val joeId = i*iFactor + j*jFactor + k*kFactor
                if (validIndices.contains(i-1)) neighbors += ((i-1)*iFactor + j*jFactor + k*kFactor)
                if (validIndices.contains(i+1)) neighbors += ((i+1)*iFactor + j*jFactor + k*kFactor)
                if (validIndices.contains(j-1)) neighbors += (i*iFactor + (j-1)*jFactor + k*kFactor)
                if (validIndices.contains(j+1)) neighbors += (i*iFactor + (j+1)*jFactor + k*kFactor)
                if (validIndices.contains(k-1)) neighbors += (i*iFactor + j*jFactor + (k-1)*kFactor)
                if (validIndices.contains(k+1)) neighbors += (i*iFactor + j*jFactor + (k+1)*kFactor)
                if ("imp3d" == topology) {
                  val allOtherNodes: List[Int] = (0 until numberOfNodes).toList.filter(x => x!=joeId && !neighbors.contains(x))
                  neighbors += allOtherNodes(Random.nextInt(allOtherNodes.length))
                }
                averageJoeRefs(joeId) ! JoeInit(neighbors.map(JoeIdToRef).toList, joeId+1, 1)
//                Thread.sleep(1000)
              }
            }
          }

          self ! DoneMakingJoes
      }

    case DoneMakingJoes =>
      algorithm match {
        case "gossip" =>
          gossipStartTime = System.currentTimeMillis()
          averageJoeRefs(Random.nextInt(numberOfNodes)) ! Rumor

        case "push-sum" =>
          gossipStartTime = System.currentTimeMillis()
          averageJoeRefs(Random.nextInt(numberOfNodes)) ! PushSum(0, 0)
      }

    case GossipTermination =>
      joesWhereGossipTerminated += sender.path.name
//      println(sender.path.name + " terminated")
      if (joesWhereGossipTerminated.length == numberOfNodes) {
        println("That's all folks!")
        println("Time taken: " + (System.currentTimeMillis() - gossipStartTime) + " milli seconds")
        if (algorithm == "push-sum") {
          context.system.shutdown()
        }
      }

    case NeighborListEmpty =>
      joesWhereNeighborListIsEmpty += sender.path.name
//      println(sender.path.name + " neighbor list empty")
      if (joesWhereNeighborListIsEmpty.length == numberOfNodes) {
//        println("gossip terminated at")
//        joesWhereGossipTerminated.foreach(printf("%s ", _))
//        println("\n")
//        println("neighbor list empty at")
//        joesWhereNeighborListIsEmpty.foreach(printf("%s ", _))
//        println("\n")
        for (i <- 0 until numberOfNodes) {
          averageJoeRefs(i) ! StatsRequest
        }
      }

    case StatsResponse(statsTo, statsFrom) =>
      statsReceived += 1
//      println("stats from " + sender.path.name)
//      statsTo.foreach(x => printf("<%d, to %s> ", x._1, x._2))
//      printf("\n")
//      statsFrom.foreach(x => printf("<%d, from %s> ", x._1, x._2))
//      println("\n")
      if (statsReceived == numberOfNodes) {
        context.system.shutdown()
      }
  }
}

class AverageJoe extends Actor with ActorLogging {

  var myNeighbors: List[ActorRef] = _
  var myActiveNeighbors: List[ActorRef] = _
  var myBoss: ActorRef = _
  var rumorsReceived: Long = 0
  val rumorLimit: Int = 10
  var tickCount: Long = 0
  val rumorsToNeighbors: ListBuffer[(Long, String)] = new ListBuffer[(Long, String)]()
  val rumorsFromNeighbors: ListBuffer[(Long, String)] = new ListBuffer[(Long, String)]()
  var isShutdown: Boolean = false
  var s: Float = 0
  var w: Float = 0
  var isConverged: Boolean = false
  val pastRatios: Queue[Float] = Queue[Float]() //check this

  def receive = {
    case JoeInit(neighborRefs, sIn, wIn) =>
      myBoss = sender
      myNeighbors = neighborRefs
      myActiveNeighbors = neighborRefs
      s = sIn
      w = wIn
      pastRatios.enqueue(0)
      pastRatios.enqueue(0)
      pastRatios.enqueue(0)
//      myNeighbors.foreach(x => log.info(x.path.name))
//      println(" ")

    case Rumor =>
      rumorsReceived += 1
      //++stat collection
      val rumorFromNeighbor = (tickCount, sender.path.name)
      rumorsFromNeighbors += rumorFromNeighbor
      //--stat collection
      if (!isConverged) {
        if (rumorsReceived == 1) self ! Tick
        else if (rumorsReceived == rumorLimit) {
          isConverged = true
          sender ! RemoveFromNeighborList
          myBoss ! GossipTermination
        }
      }
      else {
        sender ! RemoveFromNeighborList
      }

    case Tick =>
      tickCount += 1
      if (!myActiveNeighbors.isEmpty && rumorsReceived > 0) {
        val i = Random.nextInt(myActiveNeighbors.length)
        myActiveNeighbors(i) ! Rumor
        self ! Tick
        //++stat collection
        val rumorToNeighbor = (tickCount, myActiveNeighbors(i).path.name)
        rumorsToNeighbors += rumorToNeighbor
        //--stat collection
      }

    case RemoveFromNeighborList =>
      if (!myActiveNeighbors.isEmpty) {
        myActiveNeighbors = myActiveNeighbors.filter(myActiveNeighbor => myActiveNeighbor != sender)
        if (myActiveNeighbors.isEmpty)
          myBoss ! NeighborListEmpty
      }

    case PushSum(sIn, wIn) =>
      if (!isConverged) {
        s += sIn
        w += wIn
        pastRatios.dequeue
        pastRatios.enqueue(s/w)
        val pastRatiosList = pastRatios.toList
        if ((pastRatiosList(n = 1) - pastRatiosList(n = 0) < 0.0000000001) &&
          (pastRatiosList(n = 2) - pastRatiosList(n = 1) < 0.0000000001)) {
          isConverged = true
//          log.info("s/w is " + s/w)
//          sender ! RemoveFromNeighborList
          myBoss ! GossipTermination
        }
      }
      else {
//        sender ! RemoveFromNeighborList
      }

      if (!myActiveNeighbors.isEmpty) {
        s/=2
        w/=2
        val i = Random.nextInt(myActiveNeighbors.length)
        //      val rumorToNeighbor = (tickCount, myActiveNeighbors(i).path.name)
        //      rumorsToNeighbors += rumorToNeighbor
        myActiveNeighbors(i) ! PushSum(s, w)
      }


    case StatsRequest =>
      isShutdown = true
      myBoss ! StatsResponse(rumorsToNeighbors.toList, rumorsFromNeighbors.toList)

    case _ =>
      println("Remove later")
  }
}

object Project2 {

  def main (args: Array[String]) {
    var wrongUsage: Boolean = false
    val allowedTopologies: List[String] = List("full", "3d", "line", "imp3d")
    val allowedAlgorithms: List[String] = List("gossip", "push-sum")

    var numNodes: Int = 0
    var top: String = ""
    var alg: String = ""

    if(args.length == 3) {
      numNodes = args(0).toInt
      top = args(1).toLowerCase
      alg = args(2).toLowerCase

      if(!allowedTopologies.contains(top) || !allowedAlgorithms.contains(alg))
        wrongUsage = true
    }
    else wrongUsage = true

    if(wrongUsage) println("Usage: numNodes topology(=full, 3D, line, imp3D) algorithm(=gossip, push-sum)")
    else ActorSystem("Project2").actorOf(Props(new Boss), "Boss") ! BossInit(numNodes, top, alg)
  }
}
